import { Component, OnInit } from '@angular/core';
import { ClassDataService } from '../Services/class-data.service';
import { Classroom } from '../Classes/classroom';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.css']
})
export class ClassroomComponent implements OnInit {

  classrooms:Classroom[]
  classroomAdd:Classroom
  constructor(private classroomDatService:ClassDataService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getAllClassroom();
    this.classroomAdd=new Classroom();

  }

  getAllClassroom(){
    this.classroomDatService.getClassrooms().subscribe(
      response=>{
        this.classrooms=response.results;
      }
    )
  }
  addClassroom(){
    this.classroomDatService.addClassroom(this.classroomAdd).subscribe(
      response=>{
        
        this.annuler();
        this.getAllClassroom();
        this.toastr.success('avec success','Ajouter!');

      },error=>{
        
        this.toastr.error('Vérifier encore', 'Erreur');
      }
    )

  }

  annuler(){
    this.classroomAdd=new Classroom();
  }

}
