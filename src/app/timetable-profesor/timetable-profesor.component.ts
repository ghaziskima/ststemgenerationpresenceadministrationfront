import { Component, OnInit } from '@angular/core';
import { Timetable } from '../Classes/timetable';
import { EmploiDataService } from '../Services/emploi-data.service';
import { ProfesseurDataService } from '../Services/professeur-data.service';
import { Profesor } from '../Classes/profesor';
import { ToastrService } from 'ngx-toastr';
import { MettingDataService } from '../Services/metting-data.service';
@Component({
  selector: 'app-timetable-profesor',
  templateUrl: './timetable-profesor.component.html',
  styleUrls: ['./timetable-profesor.component.css']
})
export class TimetableProfesorComponent implements OnInit {
  profesors: Profesor[];
  affichier = false;
  displaytable = [];
  datatimetable;
  constructor(private timetableDataService: EmploiDataService, private toastr: ToastrService,
    private profesorDataService: ProfesseurDataService,
    private meetingservice: MettingDataService
  ) { }
  ngOnInit(): void {
    this.getAllProfessors();
  }
  getAllProfessors() {
    this.profesorDataService.getAllProfesseurs().subscribe(
      response => {
        this.profesors = response.results;
      }
    )
  }
  OnChangeProf(id) {
    this.affichier = false;
    this.meetingservice.getmeetingprof(id).subscribe(data => {
      this.displaytable = [];
      this.datatimetable = data.results;
      let null_data = { "material": null };
      if (this.datatimetable[0]) {
        this.affichier = true;
        var tab_day = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"]

       
        for (let i = 0; i < tab_day.length; i++) {
          let tab = this.datatimetable.filter(data => (data.weekdays == tab_day[i]));



        
           tab = tab.reduce((acc, current) => {
            const x = acc.find(item => item.meeting_name === current.meeting_name);
            if (!x) {
              return acc.concat([current]);
            } else {
              return acc;
            }
          }, []);

       
          
          tab.sort(this.dynamicSort("meeting_name"))
         
          let lenght_tab = tab.length
          if (lenght_tab == 0) {
            for (let k = 0; k < 7; k++) {
              tab.push(null_data);
            }
          }
           else {
             for (let j = 0; j < 7; j++) {
               if (tab[j]) {
                 while (parseInt(tab[j].meeting_name[tab[j].meeting_name.length - 1]) != j + 1) {
                   let null_data_2 = { "material2": null, "meeting_name": "S" + (parseInt(tab[j].meeting_name[tab[j].meeting_name.length - 1]) - 1).toString() };
                   tab.splice(j, 0, null_data_2);
                 
                 }
               }
               else
               {
                 let null_data_3 = { "material2": null, "meeting_name": "S" + (j+1).toString() };
                   tab.splice(j, 0, null_data_3);
               }
             }
           }
          this.displaytable.push(tab);
        }
        // console.log(this.displaytable);
      }
    })
  }


   dynamicSort(property) {
    var sortOrder = 1;

    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return function (a,b) {
        if(sortOrder == -1){
            return b[property].localeCompare(a[property]);
        }else{
            return a[property].localeCompare(b[property]);
        }        
    }
}
}
