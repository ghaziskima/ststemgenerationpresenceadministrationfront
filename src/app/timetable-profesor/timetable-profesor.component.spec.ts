import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimetableProfesorComponent } from './timetable-profesor.component';

describe('TimetableProfesorComponent', () => {
  let component: TimetableProfesorComponent;
  let fixture: ComponentFixture<TimetableProfesorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimetableProfesorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimetableProfesorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
