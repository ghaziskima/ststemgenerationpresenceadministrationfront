import { Component, OnInit } from '@angular/core';
import { ProfesseurDataService } from '../Services/professeur-data.service';
import { Profesor } from '../Classes/profesor';
import { EmploiDataService } from '../Services/emploi-data.service';
import { Timetable } from '../Classes/timetable';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.html',
  styleUrls: ['./professor.component.css']
})
export class ProfessorComponent implements OnInit {
  profesors:Profesor[];
  profesorAdd:Profesor;
  timetables:Timetable[]
  search;
  settings = {

    actions: {
      delete: false,
      edit: false,
      add: false,

     
      position: 'right', // left|right
    },
    columns: {
      
      
      nic: { 
        title: 'CIN'
      },
      first_name: {
        title: 'Nom'
      },
      second_name: {
        title: 'Prénom'
      },
      email: {
        title: 'Email'
      },
      address: {
        title: 'Address'
      }


    }
  };
  constructor(private profesorDataService:ProfesseurDataService,
    private tametableDataService:EmploiDataService,private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getAllProfessors();
    this.profesorAdd=new Profesor;
    this.getAllTimetable();
  }

  getAllProfessors(){
    this.profesorDataService.getAllProfesseurs().subscribe(
      response=>{
        this.profesors=response.results;
      }
    )

  }
  getAllTimetable(){
    this.tametableDataService.getAllTimetables().subscribe(
      response=>{
        this.timetables=response.results;
      }
    )
  }

  annuler(){
    this.profesorAdd=new Profesor;
  }
  addProfesor(){
    this.profesorDataService.addProfesseur(this.profesorAdd).subscribe(
      Response=>{
        this.annuler();
        this.getAllProfessors();
        this.toastr.success('avec success','Ajouter!');
        

      },error=>{
        
        this.toastr.error('Vérifier les données', 'Erreur');
      }
    )
  }

}
