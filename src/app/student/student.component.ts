import { Component, OnInit } from '@angular/core';
import { StudentDataService } from '../Services/student-data.service';
import { Student } from '../Classes/student';
import { ToastrService } from 'ngx-toastr';
import { GroupeDataService } from '../Services/groupe-data.service';
import { Groupe } from '../Classes/groupe';


@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
 studentAdd:Student;
 groupes:Groupe[];
 groupeSelected;
 displayGroupeBoolean=false;
 displayGroupeResult;
 search;
 pictures=[];
  constructor(private studentDataService:StudentDataService,private toastr: ToastrService,
    private groupeDataService:GroupeDataService) { }

  ngOnInit(): void {
    this.studentAdd=new Student;
    this.getAllGroupes();
  }
  addStudent(){



    let student_form_data=new FormData();
      

    student_form_data.append('first_name',this.studentAdd.first_name)
    student_form_data.append('second_name',this.studentAdd.second_name);
    student_form_data.append('nic',this.studentAdd.nic);
    student_form_data.append('birthday',this.studentAdd.birthday);
    student_form_data.append('email',this.studentAdd.email);
    student_form_data.append('address',this.studentAdd.address);
    student_form_data.append('image',this.studentAdd.image);
    student_form_data.append('groupe',this.studentAdd.groupe);
    this.studentDataService.addStudent(student_form_data).subscribe(
      response=>{
        
      
        
        this.annuler();
        
        this.toastr.success('avec success','Ajouter!');

      },error=>{
        
        this.toastr.error('Vérifier les données', 'Erreur');
      }
    )
  }
  annuler(){
    this.studentAdd=new Student;
  }
  getAllGroupes(){
    this.groupeDataService.getAllGroupes().subscribe(
      response=>{
        this.groupes=response.results;
      }
    )
  }
  displayListeEleves(){
    this.studentDataService.getStudentByGroupeById(this.groupeSelected).subscribe(
      response=>{
        this.displayGroupeResult=response.results;

 
        // console.log(this.displayGroupeResult.students[0].images[0].image,"#########")

        this.displayGroupeBoolean=true;
      }
    )
  }

  selectPicture(event){

    if (event.target.files && event.target.files.length > 0) {
      
       
          this.studentAdd.image=(event.target.files[0]);

          //console.log(event.target.files[i]);
        
      
    } else {
      this.studentAdd.image=null;
      
    }



    
    
    
  }

}
