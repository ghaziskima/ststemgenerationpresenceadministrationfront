import { Component, OnInit } from '@angular/core';
import { Profesor } from '../Classes/profesor';
import { ProfesseurDataService } from '../Services/professeur-data.service';
import { MettingDataService } from '../Services/metting-data.service';

import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  profesors: Profesor[];
  datatimetable;

  data_to_detecter;

  statemeeting;

  affiche=false;
  constructor(

    private profesorDataService: ProfesseurDataService,
    private meetingservice: MettingDataService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {

    this.getAllProfessors();
  }
  getAllProfessors() {
    this.profesorDataService.getAllProfesseurs().subscribe(
      response => {
        this.profesors = response.results;
      }
    )
  }


  OnChangeProf(id) {
   
    this.meetingservice.getmeetingprof(id).subscribe(data => {
      this.datatimetable = data.results;
    })
  }

  startmeeting()
  {
    this.affiche=false;
    // console.log(this.data_to_detecter);
    this.profesorDataService.startdetecter(this.data_to_detecter).subscribe(data=>
      {
        
        this.getresultmeeting(data)
        this.affiche=true;
        this.toastr.success('avec success','demarre');
      },error=>{
        // console.log("errror");

        this.toastr.error('error','demarre');
        
      })
    
  }

  annuler()
  {
    this.affiche=false;
    this.toastr.success('avec success','fin de la séance');
  }

  getresultmeeting(time)
  {
    setInterval(() => {

this.meetingservice.getmeetingstate(time).subscribe(data=>
  {
    this.statemeeting=data.results;
  
    
  })
      
    }, 5000);
  }
}
