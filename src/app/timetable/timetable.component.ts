import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { Meeting } from '../Classes/meeting';
import { Classroom } from '../Classes/classroom';
import { ClassDataService } from '../Services/class-data.service';
import { ToastrService } from 'ngx-toastr';
import { ProfesseurDataService } from '../Services/professeur-data.service';
import { Profesor } from '../Classes/profesor';
import { MatterDataService } from '../Services/matter-data.service';
import { Matter } from '../Classes/matter';
import { GroupeDataService } from '../Services/groupe-data.service';
import { SectionDataService } from '../Services/section-data.service';
import { Section } from '../Classes/section';
import { Groupe } from '../Classes/groupe';
import { Timetable } from '../Classes/timetable';
import { EmploiDataService } from '../Services/emploi-data.service';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.css']
})
export class TimetableComponent implements OnInit {
  Addmeeting: Meeting;
  Waitmeeeting = [];
  lundi = [];
  mardi = [];
  mercredi = [];
  jeudi = [];
  vendredi = [];
  samedi = [];
  classrooms: Classroom[];
  profesors: Profesor[];
  matters: Matter[];
  sections: Section[];
  groupes: Groupe[];
  groupechoose: Groupe[];
  Addtimetable: Timetable;
  updatemeeting_time = {
    "index": null,
    "meeting_name": null,
    "start": null,
    "end": null
  };
  all_meeting_time;
  show_table =false;
  constructor(
    private groupeDataService: GroupeDataService,
    private sectcionDataService: SectionDataService,
    private classroomDatService: ClassDataService,
    private profesorDataService: ProfesseurDataService,
    private matterDataService: MatterDataService,
    private emploiDataService:EmploiDataService,
    private toastr: ToastrService) { }
  ngOnInit(): void {
    this.Addmeeting = new Meeting();
    for (let i = 0; i < 7; i++) {
      this.lundi.push({ "material": null ,"weekdays":"lundi","meeting_time":"S" + (i + 1).toString(),"start": "08:00","end": "10:00"});
      this.mardi.push({ "material": null,"weekdays":"mardi","meeting_time":"S" + (i + 1).toString(),"start": "08:00","end": "10:00" });
      this.mercredi.push({ "material": null,"weekdays":"mercredi","meeting_time":"S" + (i + 1).toString(),"start": "08:00","end": "10:00" });
      this.jeudi.push({ "material": null,"weekdays":"jeudi","meeting_time":"S" + (i + 1).toString(),"start": "08:00","end": "10:00" });
      this.vendredi.push({ "material": null,"weekdays":"vendredi","meeting_time":"S" + (i + 1).toString(),"start": "08:00","end": "10:00" });
      this.samedi.push({ "material": null ,"weekdays":"samedi","meeting_time":"S" + (i + 1).toString(),"start": "08:00","end": "10:00"});
    }
    this.Addtimetable = new Timetable();
    this.getAllClassroom();
    this.getAllSection();
    this.getAllGroupes();
    this.getallmatters();
    this.getAllProfessors();
    this.getAllClassroom();
    this.groupechoose = null;
    this.show_table=false;
    //localStorage.clear();
    this.all_meeting_time = JSON.parse(localStorage.getItem("meeting_time"));
    if (!this.all_meeting_time) {
      this.all_meeting_time = [];
      for (let i = 0; i < 8; i++) {
        var meeting_time = {
          "index": null,
          "meeting_name": null,
          "start": "08:00",
          "end": "10:00"
        };
        meeting_time.index = i;
        meeting_time.meeting_name = "S" + (i + 1).toString();
        this.all_meeting_time.push(meeting_time);
      }
      localStorage.setItem("meeting_time", JSON.stringify(this.all_meeting_time));
    }
  }
  addmeeting() {
    this.Waitmeeeting.push(this.Addmeeting);
    this.Addmeeting = new Meeting();
  }

  OnChangeGroupe(x)
  {
    
    // console.log(x)
      this.emploiDataService.getTimetablebyidgroupe(x).subscribe(data=>
        {
          // console.log(data)
          if(data.results.length>0  && data.results[0].meeting2)
          {
            for(let x of data.results[0].meeting2)
            {
              x['material']=x['material2']
              x['classroom']=x['classroom2']
              x['professor']=x['professor2']
            }
         
              this.lundi=data.results[0].meeting2.filter(x=>(x.weekdays=="lundi"));
              this.mardi=data.results[0].meeting2.filter(x=>(x.weekdays=="mardi"));
              this.mercredi=data.results[0].meeting2.filter(x=>(x.weekdays=="mercredi"));
              this.jeudi=data.results[0].meeting2.filter(x=>(x.weekdays=="jeudi"));
              this.vendredi=data.results[0].meeting2.filter(x=>(x.weekdays=="vendredi"));
              this.samedi=data.results[0].meeting2.filter(x=>(x.weekdays=="samedi"));
  
             
             
           
          }
          // console.log("true")
          this.show_table=true;
          
        },error=>
        {
          // console.log("false")
            this.Addtimetable.groupe=null;
            this.show_table=false;
      
        })
    
     
    
    
   
    
  }
  //event.previousIndex//old index
  // event.currentIndex//new index
  onDrop(event: CdkDragDrop<Meeting[]>) {
    if (event.previousContainer === event.container) // in the same containner
    {
      moveItemInArray(event.container.data,
        event.previousIndex,
        event.currentIndex);
      ///////////////////////////////////////
      for (let i = 0; i < 7; i++) {
        event.container.data[i].end = this.all_meeting_time[i].end;
        event.container.data[i].start = this.all_meeting_time[i].start;
        event.container.data[i].meeting_name = this.all_meeting_time[i].meeting_name;
        event.container.data[i].weekdays = event.previousContainer.element.nativeElement.id;
        ////////////////////////////////////////
      }
      ////////////////////////////////////////
      // console.log("same");
    } else//out from his container
    {
      if (event.previousContainer.data[event.previousIndex].material) {
        if (event.container.element.nativeElement.id == "Waitmeeeting") {
          // console.log("to wait ");
          event.container.data.push(event.previousContainer.data[event.previousIndex]);
          event.previousContainer.data.splice(event.previousIndex, 1);
          let meeting_vide = new Meeting();
          meeting_vide.material = null;
          meeting_vide.professor = null;
          meeting_vide.classroom = null;
          meeting_vide.end=this.all_meeting_time[event.previousIndex ].end;
          meeting_vide.start=this.all_meeting_time[event.previousIndex ].start;
          meeting_vide.weekdays= event.previousContainer.element.nativeElement.id;
          meeting_vide.meeting_name=this.all_meeting_time[event.previousIndex ].meeting_name;
          event.previousContainer.data.splice(event.previousIndex + 1, 0, meeting_vide);
        }
        else if (!event.container.data[event.currentIndex].material) {
          // console.log("to vide");
          event.container.data.splice(event.currentIndex, 1);
          let meeting_vide = new Meeting();
          meeting_vide.material = null;
          meeting_vide.professor = null;
          meeting_vide.classroom = null;
          meeting_vide.end=this.all_meeting_time[event.previousIndex ].end;
          meeting_vide.start=this.all_meeting_time[event.previousIndex ].start;
          meeting_vide.weekdays= event.previousContainer.element.nativeElement.id;
          meeting_vide.meeting_name=this.all_meeting_time[event.previousIndex ].meeting_name;
          if (event.previousContainer.element.nativeElement.id != "Waitmeeeting") {
            event.previousContainer.data.splice(event.previousIndex + 1, 0, meeting_vide);
          }
          transferArrayItem(event.previousContainer.data,
            event.container.data,
            event.previousIndex, event.currentIndex);
          ///////////////////////////////////////
          for (let i = 0; i < 7; i++) {
            event.container.data[i].end = this.all_meeting_time[i].end;
            event.container.data[i].start = this.all_meeting_time[i].start;
            event.container.data[i].meeting_name = this.all_meeting_time[i].meeting_name;
            event.container.data[i].weekdays = event.container.element.nativeElement.id;
            ////////////////////////////////////////
          }
          ////////////////////////////////////////
        }
        else if (event.container.data[event.currentIndex].material && event.container.data[event.currentIndex].material != undefined) {
          // console.log("to material ");
          let in_to = event.previousContainer.data[event.previousIndex];
          let ou_from = event.container.data[event.currentIndex];
          event.previousContainer.data[event.previousIndex] = ou_from;//in
          event.container.data[event.currentIndex] = in_to;//out
          ///////////////////////////////////////
          for (let i = 0; i < 7; i++) {
            event.container.data[i].end = this.all_meeting_time[i].end;
            event.container.data[i].start = this.all_meeting_time[i].start;
            event.container.data[i].meeting_name = this.all_meeting_time[i].meeting_name;
            event.container.data[i].weekdays = event.container.element.nativeElement.id;
            event.previousContainer.data[i].end = this.all_meeting_time[i].end;
            event.previousContainer.data[i].start = this.all_meeting_time[i].start;
            event.previousContainer.data[i].meeting_name = this.all_meeting_time[i].meeting_name;
            event.previousContainer.data[i].weekdays = event.previousContainer.element.nativeElement.id;
            ////////////////////////////////////////
          }
          ////////////////////////////////////////
        }
      }
      // console.log("out");
    }
  }
  getAllClassroom() {
    this.classroomDatService.getClassrooms().subscribe(
      response => {
        this.classrooms = response.results;
      }
    )
  }
  getAllProfessors() {
    this.profesorDataService.getAllProfesseurs().subscribe(
      response => {
        this.profesors = response.results;
      }
    )
  }
  getallmatters() {
    this.matterDataService.getAllMatters().subscribe(data => {
      this.matters = data.results;
    })
  }
  getAllSection() {
    this.sectcionDataService.getAllSections().subscribe(
      response => {
        this.sections = response.results
      }
    )
  }
  getAllGroupes() {
    this.groupeDataService.getAllGroupes().subscribe(
      response => {
        this.groupes = response.results;
      }
    )
  }
  OnChangeSection(id) {
    this.groupechoose = this.groupes.filter(data => (data.section == id));
  }
  updatemeetingtime() {
    this.all_meeting_time[this.updatemeeting_time.index] = this.updatemeeting_time;
    localStorage.setItem("meeting_time", JSON.stringify(this.all_meeting_time));
    this.all_meeting_time = JSON.parse(localStorage.getItem("meeting_time"));
  }
  OnChangeUpdateMeetingTime(x) {
    this.updatemeeting_time = this.all_meeting_time[x];
  }
  exitupdatetimetable()
  {
    this.Addtimetable=new Timetable();
    this.Waitmeeeting = [];
    this.lundi = [];
    this.mardi = [];
    this.mercredi = [];
    this.jeudi = [];
    this.vendredi = [];
    this.samedi = [];
    this.ngOnInit();
  }
  updatetimetable()
  {
    let tab=[this.lundi,this.mardi,this.mercredi,this.jeudi,this.vendredi,this.samedi]
    this.Addtimetable.meeting=[];
    for(let i =0;i<tab.length;i++)
    {
      for(let j=0;j<tab[i].length;j++)
      {
        this.Addtimetable.meeting.push(tab[i][j])
      }
    }
    // console.log(this.Addtimetable);
    this.emploiDataService.addTimetable(this.Addtimetable).subscribe(data=>{
      // console.log(data);

      this.toastr.success('avec success','eregistrement!');
    },error=>{
      // console.log('eeeeee');
    })
  }
}
