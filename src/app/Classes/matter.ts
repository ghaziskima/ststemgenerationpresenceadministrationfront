
import { Section } from './section';

export class Matter {
    public id:number;
    public material_name:String;
    public types:String;
    public section:Section;

}
