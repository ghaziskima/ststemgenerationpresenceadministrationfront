import { Matter } from './matter';
import { Classroom } from './classroom';
import { Time } from '@angular/common';
import { Profesor } from './profesor';
export class Meeting {
    public id: number;
    public meeting_name: String;
    public start: Time;
    public end: Time;
    public material: Matter;
    public classroom: number;
    public weekdays: String;
    public diet: string;
    public types: string;
    public classrooms: Classroom;
    public professor:Profesor;
}
