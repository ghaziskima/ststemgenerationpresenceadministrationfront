import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { AccueilComponent } from './accueil/accueil.component';
import { TimetableProfesorComponent } from './timetable-profesor/timetable-profesor.component';
import { TimetableStudentComponent } from './timetable-student/timetable-student.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfessorComponent } from './professor/professor.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { StudentComponent } from './student/student.component';
import { GroupeComponent } from './groupe/groupe.component';
import { MatterComponent } from './matter/matter.component';
import { ClassroomComponent } from './classroom/classroom.component';
import { TimetableComponent } from './timetable/timetable.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DemoComponent } from './demo/demo.component';
@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    AccueilComponent,
    TimetableProfesorComponent,
    TimetableStudentComponent,
    ProfessorComponent,
    StudentComponent,
    GroupeComponent,
    MatterComponent,
    ClassroomComponent,
    TimetableComponent,
    DemoComponent,
  ],
  imports: [
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut:1000
    }),// ToastrModule added
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    AppRoutingModule,
    DragDropModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
