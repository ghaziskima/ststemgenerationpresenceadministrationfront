import { Component, OnInit } from '@angular/core';
import { Timetable } from '../Classes/timetable';
import { EmploiDataService } from '../Services/emploi-data.service';

import { ToastrService } from 'ngx-toastr';
import { SectionDataService } from '../Services/section-data.service';
import { GroupeDataService } from '../Services/groupe-data.service';
import { Groupe } from '../Classes/groupe';
import { Section } from '../Classes/section';


@Component({
  selector: 'app-timetable-student',
  templateUrl: './timetable-student.component.html',
  styleUrls: ['./timetable-student.component.css']
})
export class TimetableStudentComponent implements OnInit {

  sections: Section[];
  groupes: Groupe[];
  groupechoose: Groupe[];
  affichier = false;
  displaytable = [];
  datatimetable;
  constructor(private timetableDataService: EmploiDataService, private toastr: ToastrService,
    private sectionDataService: SectionDataService,
    private groupeDataService: GroupeDataService
  ) { }

  ngOnInit(): void {
    this.getAllSection();
    this.getAllGroupes();
 

  }

  getAllSection() {
    this.sectionDataService.getAllSections().subscribe(
      response => {
        this.sections = response.results
      }
    )
  }
  getAllGroupes() {
    this.groupeDataService.getAllGroupes().subscribe(
      response => {
        this.groupes = response.results;
      }
    )
  }
  OnChangeSection(id) {

    this.affichier = false;
    this.groupechoose = this.groupes.filter(data => (data.section == id));
  }
  OnChangeGroupe(id) {

    this.affichier = false;
    this.timetableDataService.getTimetablebyidgroupe(id).subscribe(data => {

      this.displaytable = [];

      this.datatimetable = data.results[0];
      // console.log(this.datatimetable);
      
      if (this.datatimetable) {
        this.affichier = true;

        var tab_day = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"]

        for (let i = 0; i < tab_day.length; i++) {
          this.displaytable.push(this.datatimetable.meeting2.filter(data => (data.weekdays == tab_day[i])));
        }

        // console.log(this.displaytable);




      }
     
    })
  }

}