import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AccueilComponent } from './accueil/accueil.component';
import { TimetableProfesorComponent } from './timetable-profesor/timetable-profesor.component';
import { TimetableStudentComponent } from './timetable-student/timetable-student.component';
import { ProfessorComponent } from './professor/professor.component';
import { StudentComponent } from './student/student.component';
import { GroupeComponent } from './groupe/groupe.component';
import { MatterComponent } from './matter/matter.component';
import { ClassroomComponent } from './classroom/classroom.component';
import { TimetableComponent } from './timetable/timetable.component';
import { DemoComponent } from './demo/demo.component';



const routes: Routes = [
  {
    path:'',
    component:LayoutComponent,
    children:[
      {
        path:'',
        component:AccueilComponent
      },
      {
        path:'accueil',
        component:AccueilComponent
      },
      {
        path:'emploiProf',
        component:TimetableProfesorComponent
      },
      {
        path:'emploiEleve',
        component:TimetableStudentComponent
      },
      {
        path:'professeurs',
        component:ProfessorComponent
      },
      {
        path:'eleves',
        component:StudentComponent
      },
      {
        path:'groupes',
        component:GroupeComponent
      },
      {
        path:'matieres',
        component:MatterComponent
      },
      {
        path:'salles',
        component:ClassroomComponent
      } ,
      
      {
        path:'emploi',
        component:TimetableComponent
      }
      ,
      
      {
        path:'demo',
        component:DemoComponent
      }
     
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
