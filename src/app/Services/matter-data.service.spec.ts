import { TestBed } from '@angular/core/testing';

import { MatterDataService } from './matter-data.service';

describe('MatterDataService', () => {
  let service: MatterDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatterDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
