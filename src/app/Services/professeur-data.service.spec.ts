import { TestBed } from '@angular/core/testing';

import { ProfesseurDataService } from './professeur-data.service';

describe('ProfesseurDataService', () => {
  let service: ProfesseurDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfesseurDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
