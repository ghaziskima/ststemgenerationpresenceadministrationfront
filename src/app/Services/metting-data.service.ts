import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meeting } from '../Classes/meeting';
import { Results } from '../Classes/results';

@Injectable({
  providedIn: 'root'
})
export class MettingDataService {

  private BASE_URL=window["cfgApiBaseUrl"];

  
  constructor(private http:HttpClient) { }

  addMeeting(Meeting){
    return this.http.post<Meeting>(this.BASE_URL+'/meetings/',Meeting);
  }
  getAllMeetings(){
    return this.http.get<Results>(this.BASE_URL+'/meetings/');
  }
  getMeetingById(id){
    return this.http.get<Meeting>(this.BASE_URL+'/meetings/'+id+'/');
  }
  deleteMeeting(id){
    return this.http.delete(this.BASE_URL+'/meetings/'+id+'/');
  }
  getmeetingprof(id)
  {
    return this.http.get<Results>(this.BASE_URL+'/meetings/?professor='+id);
  }

  getmeetingstate(time)
  {
    return this.http.get<Results>(this.BASE_URL+'/states/?daydate='+time);
  }
}
