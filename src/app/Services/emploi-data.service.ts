import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Timetable } from '../Classes/timetable';
import { Results } from '../Classes/results';

@Injectable({
  providedIn: 'root'
})
export class EmploiDataService {
  private BASE_URL=window["cfgApiBaseUrl"];

  
  constructor(private http:HttpClient) { }

  addTimetable(timetable){
    return this.http.post(this.BASE_URL+'/timetables/',timetable);
  }
  getAllTimetables(){
    return this.http.get<Results>(this.BASE_URL+'/timetables/');
  }
  getTimetableById(id){
    return this.http.get<Timetable>(this.BASE_URL+'/timetables/'+id+'/');
  }
  deleteTimetable(id){
    return this.http.delete(this.BASE_URL+'/timetables/'+id+'/');
  }

  getTimetablebyidgroupe(id)
  {
    return this.http.get<Results>(this.BASE_URL+'/timetables/?groupe='+id);
  }
}
