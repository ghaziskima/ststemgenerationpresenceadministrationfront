import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Results } from '../Classes/results';
import { Groupe } from '../Classes/groupe';

@Injectable({
  providedIn: 'root'
})
export class GroupeDataService {
  private BASE_URL=window["cfgApiBaseUrl"];

  
  constructor(private http:HttpClient) { }

  addGroupe(groupe){
    return this.http.post(this.BASE_URL+'/groupes/',groupe);
  }
  getAllGroupes(){
    return this.http.get<Results>(this.BASE_URL+'/groupes/');
  }
  getGroupeById(id){
    return this.http.get<Groupe>(this.BASE_URL+'/groupes/'+id+'/');
  }
  deleteGroupe(id){
    return this.http.delete(this.BASE_URL+'/groupes/'+id+'/');
  }
}
