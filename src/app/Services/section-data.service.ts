import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Results } from '../Classes/results';
import { Section } from '../Classes/section';

@Injectable({
  providedIn: 'root'
})
export class SectionDataService {
  private BASE_URL=window["cfgApiBaseUrl"];

  
  constructor(private http:HttpClient) { }

  addSection(Section){
    return this.http.post(this.BASE_URL+'/sections/',Section);
  }
  getAllSections(){
    return this.http.get<Results>(this.BASE_URL+'/sections/');
  }
  getSectionById(id){
    return this.http.get<Section>(this.BASE_URL+'/sections/'+id+'/');
  }
  deleteSection(id){
    return this.http.delete(this.BASE_URL+'/sections/'+id+'/');
  }
}
