import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Matter } from '../Classes/matter';
import { Results } from '../Classes/results';

@Injectable({
  providedIn: 'root'
})
export class MatterDataService {
  private BASE_URL=window["cfgApiBaseUrl"];

  
  constructor(private http:HttpClient) { }

  addMatter(Matter){
    return this.http.post<Matter>(this.BASE_URL+'/Materials/',Matter);
  }
  getAllMatters(){
    return this.http.get<Results>(this.BASE_URL+'/Materials/');
  }
  getMatterById(id){
    return this.http.get<Matter>(this.BASE_URL+'/Materials/'+id+'/');
  }
  deleteMatter(id){
    return this.http.delete(this.BASE_URL+'/Materials/'+id+'/');
  }
}
