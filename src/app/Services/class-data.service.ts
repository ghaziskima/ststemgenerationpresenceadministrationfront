import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Classroom } from '../Classes/classroom';
import { Results } from '../Classes/results';

@Injectable({
  providedIn: 'root'
})
export class ClassDataService {

  private BASE_URL=window["cfgApiBaseUrl"];

  
  constructor(private http:HttpClient) { }

  addClassroom(classroom){
    return this.http.post<Classroom>(this.BASE_URL+'/classrooms/',classroom);
  }
  getClassrooms(){
    return this.http.get<Results>(this.BASE_URL+'/classrooms/');
  }
  getClassroomById(id){
    return this.http.get<Classroom>(this.BASE_URL+'/classrooms/'+id+'/');
  }
  deleteClassroom(id){
    return this.http.delete(this.BASE_URL+'/classrooms/'+id+'/');
  }
}
