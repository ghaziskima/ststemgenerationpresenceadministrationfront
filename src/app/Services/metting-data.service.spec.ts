import { TestBed } from '@angular/core/testing';

import { MettingDataService } from './metting-data.service';

describe('MettingDataService', () => {
  let service: MettingDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MettingDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
