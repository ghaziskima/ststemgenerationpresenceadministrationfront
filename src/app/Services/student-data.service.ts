import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Student } from '../Classes/student';
import { Results } from '../Classes/results';

@Injectable({
  providedIn: 'root'
})
export class StudentDataService {
  private BASE_URL=window["cfgApiBaseUrl"];

  constructor(private http:HttpClient) { }

  addStudent(Student){
    return this.http.post<Student>(this.BASE_URL+'/students/',Student);
  }
  getAllStudents(){
    return this.http.get<Results>(this.BASE_URL+'/students/');
  }
  getStudentById(id){
    return this.http.get<Student>(this.BASE_URL+'/students/'+id+'/');
  }
  deleteStudent(id){
    return this.http.delete(this.BASE_URL+'/students/'+id+'/');
  }

  getStudentByGroupeById(id){
    return this.http.get<Results>(this.BASE_URL+'/students/?groupe='+id);
  }
  
}
