import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Profesor } from '../Classes/profesor';
import { Results } from '../Classes/results';

@Injectable({
  providedIn: 'root'
})
export class ProfesseurDataService {

  private BASE_URL=window["cfgApiBaseUrl"];

  constructor(private http:HttpClient) { }

  addProfesseur(profesor){
    return this.http.post(this.BASE_URL+'/professors/',profesor);
  }
  getAllProfesseurs(){
    return this.http.get<Results>(this.BASE_URL+'/professors/');
  }
  getProfesseurById(id){
    return this.http.get<Profesor>(this.BASE_URL+'/professors/'+id+'/');
  }
  deleteProfesseur(id){
    return this.http.delete(this.BASE_URL+'/professors/'+id+'/');
  }


  startdetecter(data){
    return this.http.post(this.BASE_URL+'/startdetecter/',data);
  }
}
