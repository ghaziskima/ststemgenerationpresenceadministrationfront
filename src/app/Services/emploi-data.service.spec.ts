import { TestBed } from '@angular/core/testing';

import { EmploiDataService } from './emploi-data.service';

describe('EmploiDataService', () => {
  let service: EmploiDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmploiDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
