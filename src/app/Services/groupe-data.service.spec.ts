import { TestBed } from '@angular/core/testing';

import { GroupeDataService } from './groupe-data.service';

describe('GroupeDataService', () => {
  let service: GroupeDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GroupeDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
