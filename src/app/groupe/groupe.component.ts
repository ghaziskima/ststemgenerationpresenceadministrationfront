import { Component, OnInit } from '@angular/core';
import { GroupeDataService } from '../Services/groupe-data.service';
import { Groupe } from '../Classes/groupe';
import { SectionDataService } from '../Services/section-data.service';
import { EmploiDataService } from '../Services/emploi-data.service';
import { Section } from '../Classes/section';
import { Timetable } from '../Classes/timetable';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-groupe',
  templateUrl: './groupe.component.html',
  styleUrls: ['./groupe.component.css']
})
export class GroupeComponent implements OnInit {
  groupes:Groupe[]
  groupeAdd:Groupe;
  sections:Section[]
  timetables:Timetable[]
  sectionAdd:Section
  constructor(private groupeDataService:GroupeDataService,
    private sectcionDataService:SectionDataService,
    private toastr: ToastrService,private timetableDataService:EmploiDataService) { }

  ngOnInit(): void {
    this.getAllGroupes();
    this.groupeAdd=new Groupe
    this.sectionAdd=new Section
    this.getAllSection()
    this.getAllTimeTable();
  }

  getAllGroupes(){
    this.groupeDataService.getAllGroupes().subscribe(
      response=>{
        this.groupes=response.results;
        // console.log(this.groupes);
      }
    )
  }
  getAllSection(){
    this.sectcionDataService.getAllSections().subscribe(
      response=>{
        this.sections=response.results
      }
    )

  }
  getAllTimeTable(){
    this.timetableDataService.getAllTimetables().subscribe(
      response=>{
        this.timetables=response.results;
       
        
      }
    )
  }

  addGroupe(){
    this.groupeDataService.addGroupe(this.groupeAdd).subscribe(
      response=>{
        
        this.annuler();
        this.getAllGroupes();
        this.toastr.success('avec success','Ajouter!');

      },error=>{
        
        this.toastr.error('Vérifier encore', 'Erreur');
      }
    )
  }
  annuler(){
    this.groupeAdd=new Groupe;
    this.sectionAdd=new Section;
  }
  addSection(){
    this.sectcionDataService.addSection(this.sectionAdd).subscribe(
      response=>{
        
        this.annuler();
        this.getAllSection();
        this.toastr.success('avec success','Ajouter!');

      },error=>{
        
        this.toastr.error('Vérifier encore', 'Erreur');
      }
    )
  }

}
