import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  page
  constructor() { }

  ngOnInit(): void {
    this.page=1
  }

  openPage(page){
    this.page=page;
  }

}
