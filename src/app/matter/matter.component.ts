import { Component, OnInit } from '@angular/core';
import { SectionDataService } from '../Services/section-data.service';
import { Section } from '../Classes/section';
import { Matter } from '../Classes/matter';
import { MatterDataService } from '../Services/matter-data.service';
import { ToastrService } from 'ngx-toastr';
import { EmploiDataService } from '../Services/emploi-data.service';
import { Timetable } from '../Classes/timetable';

@Component({
  selector: 'app-matter',
  templateUrl: './matter.component.html',
  styleUrls: ['./matter.component.css']
})
export class MatterComponent implements OnInit {
  sections:Section[]
  matterAdd:Matter
  search
  matters;
  timetables:Timetable[]
  timetableSelected;
  constructor( private sectcionDataService:SectionDataService,private timetableDataService:EmploiDataService,
    private matterDataService:MatterDataService,private toastr: ToastrService) { }

  ngOnInit(): void {
    this.matterAdd=new Matter()
    this.getAllSection();
    this.getAllTabletime();
    this.getallmatters();
   

  }


  getAllSection(){
    this.sectcionDataService.getAllSections().subscribe(
      response=>{
        this.sections=response.results
      }
    )

  }

  annuler(){
    
    this.matterAdd.material_name=null;
    this.matterAdd.types=null;
   
    
  }
  addMatter(){
    this.matterDataService.addMatter(this.matterAdd).subscribe(
      response=>{
         this.getTableMatter(response.section);
         this.getallmatters();
        this.annuler();
       
        this.toastr.success('avec success','Ajouter!');

      },error=>{
        
        this.toastr.error('Vérifier encore', 'Erreur');
      }
    )
  }
  getTableMatter(matterId){
    this.sectcionDataService.getSectionById(matterId).subscribe(
      response=>{
       // this.matters=response.materials;
      }
    )
    
  }

  getAllTabletime(){
    this.timetableDataService.getAllTimetables().subscribe(
      response=>{
        this.timetables=response.results;
      }
    )
  }
  selectedTimeTable(id){
    /*this.matterAdd.timetables=new Array<Timetable>()
    this.matterAdd.timetables.push(id);*/
   
    
  }

  getallmatters()
  {
   this.matterDataService.getAllMatters().subscribe(data=>{
     this.matters=data.results;
   })
  }
}
